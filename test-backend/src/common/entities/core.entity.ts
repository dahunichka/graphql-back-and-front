import { Field, ObjectType } from '@nestjs/graphql';
import { Column, CreateDateColumn, PrimaryGeneratedColumn, UpdateDateColumn } from 'typeorm';

@ObjectType()
export class CoreEntity {
  @PrimaryGeneratedColumn()
  @Field((type) => Number)
  id: number;

/*   @CreateDateColumn()
  @Field((type) => Date)
  createdAt: Date;

  @Column("timestamp with time zone", { name: "updatedAt" })
  @Field((type) => Date)
  updatedAt: Date; */
}
